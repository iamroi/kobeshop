<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->enum('status', ['pending-payment', 'failed', 'processing', 'completed', 'on-hold', 'cancelled', 'refunded']);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

//        Pending payment – Order received, no payment initiated. Awaiting payment (unpaid).
//        Failed – Payment failed or was declined (unpaid). Note that this status may not show immediately and instead show as Pending until verified (e.g., PayPal).
//        Processing – Payment received (paid) and stock has been reduced; order is awaiting fulfillment. All product orders require processing, except those that only contain products which are both Virtual and Downloadable.
//        Completed – Order fulfilled and complete – requires no further action.
//        On-Hold – Awaiting payment – stock is reduced, but you need to confirm payment.
//        Cancelled – Cancelled by an admin or the customer – stock is increased, no further action required.
//        Refunded – Refunded by an admin – no further action required.

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
