<?php

use Illuminate\Database\Seeder;
class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create 10 products using the product factory
        factory(App\Models\Order::class, 10)->create();

        // Get all the products attaching up to 3 random products to each order
        $products = App\Models\Product::all();

        // Populate the pivot table
        App\Models\Order::all()->each(function ($order) use ($products) {
            $order->products()->attach(
                $products->random(rand(1, 10))->pluck('id')->toArray()
            );
        });

        /*App\Models\Product::all()->each(function ($product) use ($orders) {
            $product->orders()->attach(
                $orders->random(rand(1, 10))->pluck('id')->toArray()
            );
        });*/
    }
}

