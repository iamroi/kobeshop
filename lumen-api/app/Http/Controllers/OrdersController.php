<?php

namespace App\Http\Controllers;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\QueryBuilder\QueryBuilder;

class OrdersController extends Controller
{
    /**
     * Display a listing of products.
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $ordersQuery = QueryBuilder::for(Order::class, $request)
                        ->with('products')
                        ->allowedFilters('name');

        // fetch only user owned order if auth user is not admin
        if(!$request->auth->hasPermissionTo('manage-orders')) {
            $ordersQuery->where('user_id', $request->auth->id);
        }

        $orders = $ordersQuery->paginate($request->limit);

        return responder()->success($orders)->respond();
    }

    public function get($id)
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param $request
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cartProducts'     => 'required',
        ]);

        $order = new Order(['status' => 'pending-payment', 'user_id' => $request->auth->id]);
        $order->save();

        $order->products()->attach($request->cartProducts);

        return responder()->success($order)->respond();
    }

    /**
     * Update the specified resource
     *
     * @param $request
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        print_r($request->auth->can('manage-orders'));
        if($request->auth->cannot('manage-orders')) {
            return responder()->error()->respond(Response::HTTP_UNAUTHORIZED);
        }

        $orderFill = $request->only(['status']);

        $order = Order::findOrFail($id);

//        print_r($order);

        $order->fill($orderFill);

        $order->save();

        return responder()->success($order)->respond(Response::HTTP_ACCEPTED);
    }
}
