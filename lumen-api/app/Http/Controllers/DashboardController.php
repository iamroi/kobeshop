<?php

namespace App\Http\Controllers;

//use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Spatie\QueryBuilder\QueryBuilder;

class DashboardController extends Controller
{
    public function __construct()
    {
        //  $this->middleware('auth:api');
    }

    /**
     * Display a listing of the users.
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$request->auth->getAllPermissions();

        $usersCount = User::count();
        $productsCount = 0;

        // get products count for admins
        if($request->auth->hasPermissionTo('edit-products')) {
            $productsCount = Product::count();
        }

        // get all orders count for admins
        // get own orders count for other users
        if($request->auth->hasPermissionTo('manage-orders')) {
            $ordersCount = Order::count();
        } else {
            $ordersCount = Order::where('user_id', $request->auth->id)->count();
        }

        $data = ['usersCount' => $usersCount, 'productsCount' => $productsCount, 'ordersCount' => $ordersCount];
        return responder()->success($data)->respond();
    }
}
