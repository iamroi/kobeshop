<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ProductsController extends Controller
{
    /**
     * Display a listing of products.
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = QueryBuilder::for(Product::class, $request)
                    ->allowedFilters('name')
                    ->paginate($request->limit);

        return responder()->success($products)->respond();
    }

    public function get($id)
    {
//        return Product::where('id', '=', $id)->with('pictures')->get();
    }
}
