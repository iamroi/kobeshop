<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    public function orders()
    {
        return $this->belongsToMany('App\Models\Order', 'product_orders')->withPivot('quantity');
    }
}
