<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_orders')->withPivot('quantity');
    }
}
