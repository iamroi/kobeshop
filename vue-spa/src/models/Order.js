import Model from './Model'

export default class Order extends Model {
    resource()
    {
        return 'orders';
    }
}
