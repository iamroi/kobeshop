// import { LOGIN, LOGOUT } from '../mutation-types'

const state = {
    // cartCount: 0,
    cartItems: JSON.parse(localStorage.getItem('cartItems')) || {},
}

const getters = {
    getCartItems () {
        return state.cartItems;
    },
    getCartItemCount () {
        let cartCount = 0;
        for (let cartItem in state.cartItems) {
            cartCount = cartCount + state.cartItems[cartItem].quantity;
        }
        return cartCount;
    },
    getCartTotal () {
        let cartTotal = 0;
        for (let cartItem in state.cartItems) {
            cartTotal = cartTotal + (state.cartItems[cartItem].quantity * state.cartItems[cartItem].price);
        }
        return Math.round(cartTotal * 100) / 100;
    }
}

const mutations = {
    addProductToCart (state, data) {
        // console.dir(data)
        // state.auth = true;
        // state.cartItems
        if(state.cartItems.hasOwnProperty(data.product.id)) {
            // state.cartItems[data.product.id].quantity += 1;
            // let product = data.product;
            // product.quantity = data.product.quantity + 1;
            // console.dir(data.product);
            // data.product.quantity = state.cartItems[data.product.id].quantity + 1;
            // Vue.set(state.cartItems, data.product.id, data.product);
            Vue.set(state.cartItems[data.product.id], 'quantity', state.cartItems[data.product.id].quantity + 1);

            // state.cartItems[data.product.id].quantity = state.cartItems[data.product.id].quantity + 1;
        } else {
            // data.product['quantity'] = 1;
            Vue.set(state.cartItems, data.product.id, data.product);
            Vue.set(state.cartItems[data.product.id], 'quantity', 1);
            // state.cartItems[data.product.id] = data.product;
            // state.cartItems[data.product.id].quantity = 1;

            /*state.cartItems[data.product.id] = {
                name: data.product.name,
                name: data.product.name,
                quantity: 1
            };*/
        }

        localStorage.setItem('cartItems', JSON.stringify(state.cartItems));

        // this.commit('updateCartItemCount', data);
    },
    removeProductFromCart (state, data) {
        // console.dir(data)
        // state.auth = true;
        // state.cartItems
        if(state.cartItems.hasOwnProperty(data.product.id)) {
            // state.cartItems[data.product.id].remove();
            Vue.delete(state.cartItems, data.product.id);
        }

        localStorage.setItem('cartItems', JSON.stringify(state.cartItems));

        // this.commit('updateCartItemCount', data);
    },
    /*updateCartItemCount(state, data) {
        state.cartCount = 0;
        for (let cartItem in state.cartItems) {
            state.cartCount = state.cartCount + state.cartItems[cartItem].quantity;
        }
    },*/
    clearCart(state, data) {
        // state.cartItems = {};
        Vue.set(state, 'cartItems', {});

        localStorage.removeItem('cartItems');
    },
}

const actions = {
    addProductToCart ({ commit, state }, data) {
        commit('addProductToCart', data);
    },
    removeProductFromCart ({ commit, state }, data) {
        commit('removeProductFromCart', data);
    },
    clearCart ({ commit, state }, data) {
        commit('clearCart', data);
    }
}

export default {
    state,
    getters,
    mutations,
    actions,
}
