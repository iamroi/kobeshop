export default {
    items: [
        {
            name: 'Dashboard',
            url: '/dashboard',
            icon: 'icon-speedometer',
            // badge: {
            //     variant: 'primary',
            //     text: 'NEW'
            // }
            meta: {
                requiredPermission: ''
            }
        },
        {
            name: 'Users',
            url: '/users',
            icon: 'icon-user',
            meta: {
                requiredPermission: 'edit-users'
            }
        },
        {
            name: 'Products',
            url: '/products',
            icon: 'fa fa-square-o',
            meta: {
                requiredPermission: 'edit-products'
            }
        },
        {
            name: 'Orders',
            url: '/orders',
            icon: 'fa fa-reorder',
            meta: {
                requiredPermission: ''
            }
        },
    ]
}
