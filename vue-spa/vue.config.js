module.exports = {
    lintOnSave: false,
    runtimeCompiler: true,
    devServer: {
        watchOptions: {
            poll: true
        },
    },
}
